package com.domain;

/**
 * @author wanfeng
 * @date 2021/3/22 20:20
 */
public class Receive {
	private int userId;
	private int bookId;
	private int score;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
}
